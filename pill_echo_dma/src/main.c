#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/cm3/nvic.h>

static void clock_setup(void)
{
    rcc_clock_setup_in_hse_8mhz_out_24mhz();

    /* Enable GPIOA, GPIOB, GPIOC clock. */
    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_GPIOB);
    rcc_periph_clock_enable(RCC_GPIOC);

    /* Enable clocks for GPIO port B (for GPIO_USART3_TX) and USART3. */
    rcc_periph_clock_enable(RCC_USART1);

    /* Enable DMA1 clock */
    rcc_periph_clock_enable(RCC_DMA1);
}

static void usart_setup(void)
{
    /* Setup GPIO pin GPIO_USART1_TX and GPIO_USART1_RX. */
    gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
                  GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_USART1_TX);
    gpio_set_mode(GPIOA, GPIO_MODE_INPUT,
                  GPIO_CNF_INPUT_FLOAT, GPIO_USART1_RX);

    /* Setup UART parameters. */
    usart_set_baudrate(USART1, 115200);
    usart_set_databits(USART1, 8);
    usart_set_stopbits(USART1, USART_STOPBITS_1);
    usart_set_mode(USART1, USART_MODE_TX_RX);
    usart_set_parity(USART1, USART_PARITY_NONE);
    usart_set_flow_control(USART1, USART_FLOWCONTROL_NONE);

    /* Finally enable the USART. */
    usart_enable(USART1);

    nvic_set_priority(NVIC_DMA1_CHANNEL4_IRQ, 0);
    nvic_enable_irq(NVIC_DMA1_CHANNEL4_IRQ);

    nvic_set_priority(NVIC_DMA1_CHANNEL5_IRQ, 0);
    nvic_enable_irq(NVIC_DMA1_CHANNEL5_IRQ);
}

static void dma_write(char *data, int size)
{
    /*
	 * Using channel 4 for USART1_TX
	 */

    /* Reset DMA channel*/
    dma_channel_reset(DMA1, DMA_CHANNEL4);

    dma_set_peripheral_address(DMA1, DMA_CHANNEL4, (uint32_t)&USART1_DR);
    dma_set_memory_address(DMA1, DMA_CHANNEL4, (uint32_t)data);
    dma_set_number_of_data(DMA1, DMA_CHANNEL4, size);
    dma_set_read_from_memory(DMA1, DMA_CHANNEL4);
    dma_enable_memory_increment_mode(DMA1, DMA_CHANNEL4);
    dma_set_peripheral_size(DMA1, DMA_CHANNEL4, DMA_CCR_PSIZE_8BIT);
    dma_set_memory_size(DMA1, DMA_CHANNEL4, DMA_CCR_MSIZE_8BIT);
    dma_set_priority(DMA1, DMA_CHANNEL4, DMA_CCR_PL_VERY_HIGH);

    dma_enable_transfer_complete_interrupt(DMA1, DMA_CHANNEL4);

    dma_enable_channel(DMA1, DMA_CHANNEL4);

    usart_enable_tx_dma(USART1);
}

volatile int transferred = 0;

void dma1_channel4_isr(void)
{
    if ((DMA1_ISR & DMA_ISR_TCIF4) != 0)
    {
        DMA1_IFCR |= DMA_IFCR_CTCIF4;

        transferred = 1;
    }

    dma_disable_transfer_complete_interrupt(DMA1, DMA_CHANNEL4);

    usart_disable_tx_dma(USART1);

    dma_disable_channel(DMA1, DMA_CHANNEL4);
}

static void dma_read(char *data, int size)
{
    /*
	 * Using channel 5 for USART1_RX
	 */

    /* Reset DMA channel*/
    dma_channel_reset(DMA1, DMA_CHANNEL5);

    dma_set_peripheral_address(DMA1, DMA_CHANNEL5, (uint32_t)&USART1_DR);
    dma_set_memory_address(DMA1, DMA_CHANNEL5, (uint32_t)data);
    dma_set_number_of_data(DMA1, DMA_CHANNEL5, size);
    dma_set_read_from_peripheral(DMA1, DMA_CHANNEL5);
    dma_enable_memory_increment_mode(DMA1, DMA_CHANNEL5);
    dma_set_peripheral_size(DMA1, DMA_CHANNEL5, DMA_CCR_PSIZE_8BIT);
    dma_set_memory_size(DMA1, DMA_CHANNEL5, DMA_CCR_MSIZE_8BIT);
    dma_set_priority(DMA1, DMA_CHANNEL5, DMA_CCR_PL_HIGH);

    dma_enable_transfer_complete_interrupt(DMA1, DMA_CHANNEL5);

    dma_enable_channel(DMA1, DMA_CHANNEL5);

    usart_enable_rx_dma(USART1);
}

volatile int received = 0;

void dma1_channel5_isr(void)
{
    if ((DMA1_ISR & DMA_ISR_TCIF5) != 0)
    {
        DMA1_IFCR |= DMA_IFCR_CTCIF5;

        received = 1;
    }

    dma_disable_transfer_complete_interrupt(DMA1, DMA_CHANNEL5);

    usart_disable_rx_dma(USART1);

    dma_disable_channel(DMA1, DMA_CHANNEL5);
}

static void gpio_setup(void)
{
    /* Set GPIO8 (in GPIO port C) to 'output push-pull'. */
    gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO13);
}

int main(void)
{
    char echo[7] = "\n\recho ";

    char tx[100];
    int tx_len = 0;

    char rx[1];
    int rx_len = 1;

    clock_setup();
    gpio_setup();
    usart_setup();

    transferred = 0;
    dma_write(tx, tx_len);
    received = 0;
    dma_read(rx, rx_len);

    /* Blink the LED (PA8) on the board with every transmitted byte. */
    while (1)
    {
        gpio_toggle(GPIOC, GPIO13);
        while (transferred != 1)
        {
            if (received == 1)
            {
                received = 0;
                dma_read(rx, rx_len);

                if (rx[0] == '\r')
                {
                    tx[tx_len++] = '\n';
                    tx[tx_len++] = '\r';

                    dma_write(echo, 7);
                    while (transferred != 1)
                    {
                        /* code */
                    }

                    dma_write(tx, tx_len);
                    tx_len = 0;
                }
                else
                {
                    tx[tx_len++] = rx[0];
                    dma_write(rx, rx_len);
                }

                if (tx_len == 100)
                    tx_len = 0;
            }
        }
        transferred = 0;
    }

    return 0;
}
