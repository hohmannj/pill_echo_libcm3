
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/cm3/nvic.h>

void init_usart(void);
void init_clock(void);
void init_led(void);
void send_buffer(char *buffer, int bufferLenght);

char volatile data;
int volatile received = 0;

int main(void)
{
    int index = 0;
    char buffer[100];

    init_clock();
    init_led();
    init_usart();

    while (1)
    {
        if (received)
        {
            if (data == '\r')
            {
                usart_send_blocking(USART1, '\n');
                send_buffer(buffer, index + 1);
                usart_send_blocking(USART1, '\n');
                usart_send_blocking(USART1, '\r');
                index = 0;
            }
            else
            {
                buffer[index++] = 0;
                buffer[index++] = data;
            }

            received = 0;
        }
    }

    return 0;
}

void send_buffer(char *buffer, int bufferLenght)
{
    for (int i = 0; i < bufferLenght; i++)
    {
        usart_send_blocking(USART1, buffer[i]);
    }
}

void init_usart(void)
{
    // UART TX on PA9 (GPIO_USART1_TX)
    gpio_set_mode(GPIOA,
                  GPIO_MODE_OUTPUT_10_MHZ,
                  GPIO_CNF_OUTPUT_ALTFN_PUSHPULL,
                  GPIO_USART1_TX);

    usart_set_baudrate(USART1, 115200);
    usart_set_databits(USART1, 8);
    usart_set_stopbits(USART1, USART_STOPBITS_1);
    usart_set_mode(USART1, USART_MODE_TX_RX);
    usart_set_parity(USART1, USART_PARITY_NONE);
    usart_set_flow_control(USART1, USART_FLOWCONTROL_NONE);

    nvic_enable_irq(NVIC_USART1_IRQ);
    usart_enable_rx_interrupt(USART1);

    usart_enable(USART1);
}

void init_clock(void)
{
    rcc_periph_clock_enable(RCC_GPIOC);
    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_USART1);
}

void init_led(void)
{
    gpio_set_mode(GPIOC,
                  GPIO_MODE_OUTPUT_2_MHZ,
                  GPIO_CNF_OUTPUT_PUSHPULL,
                  GPIO13);

    gpio_clear(GPIOC, GPIO13);
}

void usart1_isr(void)
{
    /* Check if we were called because of RXNE. */
    if (((USART_CR1(USART1) & USART_CR1_RXNEIE) != 0) &&
        ((USART_SR(USART1) & USART_SR_RXNE) != 0))
    {
        /* Indicate that we got data. */
        gpio_toggle(GPIOA, GPIO13);

        data = usart_recv(USART1);
        received = 1;

        USART_CR1(USART1) |= USART_CR1_TXEIE;
    }

    if (((USART_CR1(USART1) & USART_CR1_TXEIE) != 0) &&
        ((USART_SR(USART1) & USART_SR_TXE) != 0))
    {
        /* Indicate that we are sending out data. */
        gpio_toggle(GPIOA, GPIO13);

        /* Put data into the transmit register. */
        usart_send(USART1, data);

        /* Disable the TXE interrupt as we don't need it anymore. */
        USART_CR1(USART1) &= ~USART_CR1_TXEIE;
    }
}