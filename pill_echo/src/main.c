
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <string.h>

void init_usart(void)
{
    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_USART1);

    // UART TX on PA9 (GPIO_USART1_TX)
    gpio_set_mode(GPIOA,
                  GPIO_MODE_OUTPUT_50_MHZ,
                  GPIO_CNF_OUTPUT_ALTFN_PUSHPULL,
                  GPIO_USART1_TX);

    usart_set_baudrate(USART1, 115200);
    usart_set_databits(USART1, 8);
    usart_set_stopbits(USART1, USART_STOPBITS_1);
    usart_set_mode(USART1, USART_MODE_TX_RX);
    usart_set_parity(USART1, USART_PARITY_NONE);
    usart_set_flow_control(USART1, USART_FLOWCONTROL_NONE);
    usart_enable(USART1);
}

static void sendBuffer(char *buffer, int bufferLenght)
{
    for (int i = 0; i < bufferLenght; i++)
    {
        usart_send_blocking(USART1, buffer[i]);
    }
}

int main(void)
{
    int index = 0;
    char c;
    char buffer[100];

    init_usart();

    while (1)
    {
        c = usart_recv_blocking(USART1);
        usart_send_blocking(USART1, c);
        if (c == '\r')
        {
            usart_send_blocking(USART1, '\n');
            buffer[index++] = '\0';
            sendBuffer(buffer, strlen(buffer));
            usart_send_blocking(USART1, '\n');
            usart_send_blocking(USART1, '\r');

            index = 0;
        }
        else
        {
            buffer[index++] = c;
        }
    }

    return 0;
}